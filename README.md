## A Naïve Bignum Implementation

*There is no doubt that Marley was dead.  This must be distinctly understood,
or nothing wonderful can come of the story I am going to relate.*

What is contained herein will not be so remarkable unless you know that it is
my attempt at a relatively efficient bignum implementation having read
absolutely nothing about how bignum libraries are actually implemented. It is
completely naïve. I just thought about the problem then wrote the code. May its
contents be instructive to those tasked with teaching the subject.
