import Data.Maybe
import Data.List
import qualified Data.Bits as B

-- The size of a single digit in bits. In reality, this would be somewhere
-- around the architecture's integer size. I have made it small for testing
-- purposes.
nbits :: Int
nbits = 4
p :: Int
p = 2^nbits

-- Digit order is least significant digit first in list, so x = sum_i x_i p^i.
--
-- This doesn't have anything to do with digit order, but as a corollary to the
-- above definition, the empty list has value zero.


-- Overflow an integer into two digits. Return (x0, x1), where x = x0 + p*x1.
overflow :: Int -> (Int, Int)
overflow x = (x B..&. (p - 1), x `B.shiftR` nbits)


-- THE SINGLE DIGIT FUNCTIONS
-- ==========================
--
-- These are the building blocks of the bignum algorithms to follow. They
-- correspond (approximately) to single instructions on the CPU.

-- Add the digits a and b, and a carry bit (either zero or one), c. Produces
-- (d0, d1). d0 is the main result and d1 is the carry, so d0 + d1*p = a + b +
-- c.
add1 :: Int -> Int -> Int -> (Int, Int)
add1 a b c = overflow (a + b + c)

-- Multiply two digits. Results in a 2-digit number in general. Returns (y0,
-- y1), where y0 + p*y1 = a*b. mul1 is going to be taken. See ahead.
mul11 :: Int -> Int -> (Int, Int)
mul11 a b = overflow (a * b)

-- Divide a two-digit number by a one-digit number, more or less. There are
-- also carry bits. Produces (q, rnewc, rnew0), where r = q*a + rnew, r =
-- rc:r0:r1, a = 1:a0, and rnew \in [0, a). rc is a carry bit, either zero
-- or one.
--
-- If r >= a:0 (i.e. rc:r0:r1 >= 1:a0:0), then the behaviour is undefined. So,
-- watch your ass. The idea is that q always fits in a single digit. That
-- guarantee can't be made if r >= a:0.
-- 
-- The variable names r and rnew were chosen because it is easiest to think of
-- the r in r//a as a remainder and rnew as a new remainder. The order of
-- arguments is big endian because divMod21 is only used by big endian
-- algorithms.
-- 
-- x86_64 defines a divmod with a 128-bit dividend spread across two 64-bit
-- registers, so this pretty much exactly corresponds to one instruction,
-- except the digit width will have to be 63, so that the all of the numbers
-- fit.
divMod21 :: Int -> Int -> Int -> Int -> (Int, Int, Int)
divMod21 rc r0 r1 a0 = (q, rnewc, rnew0) where
    (q, rnew) = Prelude.divMod (p^2*rc + p*r0 + r1) (p + a0)
    (rnew0, rnewc) = overflow rnew  -- overflow is little endian.

complement1 :: Int -> Int
complement1 x = y0 where
    (y0, y1) = overflow (B.complement x)

-- Concatenate digits a and b into a:b, then extract the digit starting at bit
-- n. On x86_64, the double shift instructions can be used to implement this
-- with just one instruction, more or less.
cat :: Int -> Int -> Int -> Int
cat a b n = y0 where
    (y0, y1) = overflow ((a `B.shiftR` n) B..|. (b `B.shiftL` (nbits - n)))

-- THE BIGNUM FUNCTIONS
-- ====================

toInt :: [Int] -> Int
toInt [] = 0
toInt (x0:xrest) = x0 B..|. ((toInt xrest) `B.shiftL` nbits)

toBignum :: Int -> [Int]
toBignum 0 = []
toBignum x = xlow : toBignum xhigh where
    (xlow, xhigh) = overflow x

-- Get rid of the trailing zeros. I don't know why, but I find this one
-- particularly sexy.
zeroClip :: [Int] -> [Int]
zeroClip [] = []
zeroClip (x0:xrest) = if   x0 == 0 && y == []
                      then []
                      else x0:y
    where y = zeroClip xrest
zeroClipBE :: [Int] -> [Int]
zeroClipBE x = dropWhile (==0) x


complement :: [Int] -> [Int]
complement x = map complement1 x


-- Add with a carry. addc a b c = a + b + c. a and b are bignums and c is a
-- carry bit, either 0 or 1.
addc :: [Int] -> [Int] -> Int -> [Int]
addc [] [] 0 = []  -- Technically not needed, but cuts out a lot of zeros.
addc [] [] c = [c]
addc [] b c = addc b [] c
addc (a0:arest) b c = d0:(addc arest brest d1) where
    (b0, brest) = fromMaybe (0, []) (uncons b)
    (d0, d1) = add1 a0 b0 c
add :: [Int] -> [Int] -> [Int]
add a b = addc a b 0

-- For now, negatives are not supported. Sorry. This one reuses addc, but it
-- doesn't stream properly due to the length call, even though it could. Also,
-- lol, subprime.
sub' :: [Int] -> [Int] -> [Int]
sub' b a = init (addc b
                      (complement (a ++ (replicate (length b) 0)))
                      1)
-- This one will stream properly. Still doesn't support negatives.
sub :: [Int] -> [Int] -> [Int]
sub b a = subc b a 1 where
    subc [] [] c = []
    subc (b0:brest) a c = d0:(subc brest arest d1) where
        (a0, arest) = fromMaybe (0, []) (uncons a)
        (d0, d1) = add1 b0 (complement1 a0) c

-- Multiply a bignum by a digit. Useful by itself, so it's getting its own
-- public name.
mul1 :: Int -> [Int] -> [Int]
mul1 a [] = []
mul1 a (b0:brest) = c0:(add [c1] (mul1 a brest)) where
    (c0, c1) = mul11 a b0

mul :: [Int] -> [Int] -> [Int]
-- Ensure b /= 0, to guarantee a non-empty c0:crest up ahead.
mul a [] = []
mul [] b = []
mul (a0:arest) b = c0:(add crest (mul arest b)) where
    c0:crest = mul1 a0 b

-- Shift high. A better name than shift left. Left?! WTF direction is that?!
shiftH :: [Int] -> Int -> [Int]
shiftH x n = let (nhigh, nlow) = Prelude.divMod n nbits
                 lowShift      = zipWith (\x0 x1 -> cat x0 x1 (nbits - nlow))
                                         (0:x) (x ++ [0])
             in zeroClip ((replicate nhigh 0) ++ lowShift)
shiftHBE :: [Int] -> Int -> [Int]
shiftHBE x n = let (nhigh, nlow) = Prelude.divMod n nbits
                   lowShift      = zipWith (\x0 x1 -> cat x1 x0 (nbits - nlow))
                                           (0:x) (x ++ [0])
               in zeroClipBE (lowShift ++ (replicate nhigh 0))
shiftLBE :: [Int] -> Int -> [Int]
shiftLBE x n = let
        (nhigh, nlow) = Prelude.divMod n nbits
        lowShift = zipWith
            (\x0 x1 -> cat x1 x0 nlow)
            (0:x)
            x
    in zeroClipBE (take ((length lowShift) - nhigh) lowShift)

-- For all of the big endian algorithms, the naming convention is that the most
-- significant bit has the lowest suffix number. This was because it is unknown
-- how long the number actually is. I might end up revising this later.

-- Subtract a from b. Does not support negative answers, so make sure b > a.
-- This function assumes its two inputs have the same length. This is the only
-- way it could stream right.
subBE :: [Int] -> [Int] -> [Int]
subBE b a = tail (addSub 0 0 b a)
    -- Mathematically, addSub c n b a = c*p**(n + length b) + b - a. addSub
    -- can be written recursively, whereas subBE can't.
    --
    -- tail is applied because the answer will always lead with a 0.
    where addSub c n [] [] = c : replicate n 0
          addSub c n (b0:br) (a0:ar) = case compare b0 a0 of
              GT -> c : replicate n 0 ++ addSub (b0 - a0) 0 br ar
              EQ -> addSub c (n + 1) br ar
              LT -> (c - 1) : replicate n (p - 1) ++
                  addSub (p + b0 - a0) 0 br ar

-- Subtract a from b returning (r, |b - a|). r is the comparison result.  This
-- one actually does support negative answers. This was made so that the work
-- done in the compare could be reused in the subtract. This function assumes
-- its two inputs have the same length. This is the only way it could stream
-- right. (Due to haskelly awesomeness, if you only use the r, then the rest
-- of the calculation is cancelled. This is why there is no dedicated cmpBE
-- function.)
cmpSubBE :: [Int] -> [Int] -> (Ordering, [Int])
cmpSubBE b a = (
        order,
        replicate nzeros 0 ++ if order == LT
            then subBE adiff bdiff
            else subBE bdiff adiff
    )
    where
    (order, nzeros, bdiff, adiff) = cmp 0 b a
    cmp nzeros [] [] = (EQ, nzeros, [], [])
    cmp nzeros b a = case compare b0 a0 of
        LT -> (LT, nzeros, b, a)
        EQ -> cmp (nzeros + 1) br ar
        GT -> (GT, nzeros, b, a)
        where
        (b0:br) = b
        (a0:ar) = a

-- Multiply a digit with a big-endian number. The answer always has length b +
-- 1 digits, so its length is predictable.
mul1BE :: Int -> [Int] -> [Int]
mul1BE a b = tail (acc 0 0 0 b) where
    -- acc c0 q c2 b = c0 F F c2 0 0 0 + a*b, where there are q Fs and length b
    -- zeros. Oh, and F = p - 1.
    acc c0 q c2 [] = c0 : replicate q (p - 1) ++ [c2]
    acc c0 q c2 (b0:br)
        | e0 == 0 && e1 == p - 1
            = acc c0 (q + 1) d1 br
        | e0 == 1 && e1 == p - 1
            = (c0 + 1) : replicate (q - 1) 0 ++ acc 0 1 d1 br
        | e0 == 0
            = c0 : replicate q (p - 1) ++ acc e1 0 d1 br
        | e0 == 1
            = (c0 + 1) : replicate q 0 ++ acc e1 0 d1 br
        where
        (d1, d0) = mul11 a b0
        (e1, e0) = add1 c2 d0 0


-- Divide r by a, bigendian.
divModBE :: [Int] -> [Int] -> ([Int], [Int])
divModBE r a = (q, shiftLBE rshnew shift) where
    -- Before the algorithm starts, r and a are shifted such that ash leads
    -- with a 1. Then in each step, the first two digits of ash, 1:a0, are
    -- used to estimate the next digit of q. This fits naturally with the
    -- definition of divMod21, which was defined to maximise the rate at which
    -- digits of q may be calculated and is a necessary part of the guarantee
    -- that one digit of q is calculated with every iteration.
    --
    -- The other part of this guarantee is that rc01 must be < 1:a0. (See
    -- divMod21.) In order to ensure that, the first time divModAdd is run, a
    -- leading zero is added to rsh.
    rz = zeroClipBE r
    az = zeroClipBE a
    ahigh = head az
    shift = (B.countLeadingZeros ahigh -
             (B.finiteBitSize ahigh - nbits) + 1) `mod` nbits
    ash = shiftHBE az shift
    rsh = shiftHBE rz shift  -- lol, rsh.
    a0 = ash !! 1  -- head ash should = 1.
    w = length ash + 1

    (q, rshnew) = divModAdd [] (0:rsh)
    -- divModAdd q r = (q + r//a, r%a).  Increment q by 1 for every subtraction
    -- of r by a until r < a. That's more or less how every divMod algorithm
    -- works. See inside for details...
    divModAdd q r = if length rc012 < w
        -- q is accumulated from most to least significant, so it's actually
        -- little endian. So, reverse it, because divModBE is big endian.
        -- The actual little endian divMod reverses it again, but I'm
        -- assuming (HOPEFULLY!) that haskell is smart enough to optimise
        -- out the double reverse.
        then (reverse q, r)
        else
            let rc:r0:r1:r2 = rc012
                (q0, sc, s0) = divMod21 rc r0 r1 a0
                (order, s) = cmpSubBE rc012 (mul1BE q0 ash)
                (q0', s') = if order == LT
                    -- Find the true next digit, q0'. It is guaranteed to be
                    -- either q0 or q0 - 1. If q0 pushes the new remainder into
                    -- the negatives, it must be q0 - 1 instead.
                    then (q0 - 1, subBE rc012 (mul1BE (q0 - 1) ash))
                    else (q0, s)
            -- s' is guaranteed to lead with a zero, so nuke it with a tail.
            in divModAdd (q0':q) (tail s' ++ r3)
        where (rc012, r3) = splitAt w r

-- Divide r by a.
divMod :: [Int] -> [Int] -> ([Int], [Int])
divMod r a = (reverse q, reverse r') where
    (q, r') = divModBE (reverse r) (reverse a)

--divMod1BE :: [Int] -> Int -> ([Int], Int)
-- I should make this for doing base conversion especially, but it is a good
-- thing to have in general.

-- I should also make big-endian base conversion for later, when I make
-- infinite-length real numbers. Oh yeah, also yeah, I should make infinite-
-- length real numbers.

-- I'm developing a theory that the shifts I do before starting divModBE are
-- useless and leave me with the same chance of requiring a restart on the
-- subtraction step, about 50/50. I could easily test the theory by
-- implementing divModBE without shifts and do a benchmark comparison.

-- If I really want to use shifts to speed the calculation, I should be using
-- them to calculate leading bits of the next q. If any one of those bits is
-- one, it proves the first q is correct. Or something like that. For this to
-- work, I would need to extend divMod21 to more of a divMod22. This would
-- reduce digit size or complicate divMod2x's implementation. And the benefit
-- would be pretty tiny, actually. The cmpSubBE is likely to fail very early on
-- if at all. Due to lazy evaluation, hardly any time would be saved getting
-- the first q right the first time.
